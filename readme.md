# 演示文稿生成器
SYSU Presentation Group @ 2018

原本做这个只是因为要做答辩的论文演示文稿，后来发现生成演示文稿太麻烦了，拖拖拖很麻烦。
后来发现了 `reveal.js` ，一个很有意思的基于`HTML`的演示文稿生成库。
然而HTML中的语法标记还是很多，个人更喜欢MD这种容易写又好看的格式，于是搞了这个工具。



## 语法和格式
主要的语法格式可以参照 `demo.md`，Markdown的语法基本全部支持了。

此处只提最重要的格式：`页分割`和`子页分割`符号：
* 页分割符号：`---`, 两个空行包围起来的三个英文横杠。
* 子页分割符号： `----`, 两个空行包围起来的四个英文横杠。

## 用法
这个生成器的用法有两个:
1. **持续集成自动构建**: 如果你选择这种方式，那么你需要做的是:
    * 将这个仓库 `Fork` 到你的个人账户中。
    * 去你的个人仓库中将fork后的仓库 `git clone` 到本地。
    * 根据你的演示文稿的内容修改`demo.md`的内容。
    * 在本地的 `dev` 或 `master` 分支提交(`commit`)，并推送(`git push`)至你的个人仓库。
    * 在你个人仓库的左侧导航栏找到 `CI/CD`选项， 选择`Pipeline` 进去查看构建结果。
    * 若构建成功则能在`Pipeline`页面找到构建好的压缩包下载链接。

    上述步骤可以使用自动构建脚本执行，但是需要注意的是：需要安装好`git`，同时需要修改文件 `run-ci-win.bat`(windows用) 或者 `run-ci-linux.sh`(mac或者linux可用) 或中的 `<Insert Git Repo Here>`， 将其替代为你的个人仓库地址。

    ​

2. **本地编译**： 如果你选择这种方式，那么你需要做的是:
    * 安装好 Node.JS 环境(Node.JS 版本 >= 6)。
    * `git clone` 本仓库。
    * `git clone` [编译脚本仓库](https://gitlab.com/pre-group/scripts-src)。
    * 在脚本仓库中安装依赖：
        * cd scripts-src
        * npm install
    * 使用脚本来编译所需要的`md文档`,下面这个就是取 md-src下的 demo.md 进行编译， 生成后将结果放置在 demo 文件夹下。
        * scripts-src/index.js md-src/demo.md --static demo

    注意此部分也可以通过自动构建脚本来完成，但是前提需要安装好 `git`(mac/linux)或者 `git-bash`(windows)和Node.js(>=6)。使用构建脚本的话，只需要下载本仓库，然后编辑md文件，之后使用 `local-build.bat`(windows) 或者 `local-build.sh`(mac/linux) 即可。生成的文件夹为 `demo`。

## 配置文件

配置文件为此仓库下的 `configs.json`, 可选的配置有：
```json
{
  "disableAutoOpen": false,
  // 高亮主题，可用的主题参见 highlight.js 官网
  "highlightTheme": "zenburn",
  "host": "localhost",
  "scripts": [],
  "css": [],
  "preprocessor": null,
  
  // 本地运行端口，用来调试用
  "port": 8888,
  
  // 输出成pdf， 如果设置为true就自动生成pdf
  "print": false,

  // reveal.js 的设置， 这里只设了transition。
  // transition 一共有六种，为切换幻灯片时的效果
  // none/fade/slide/convex/concave/zoom
  "revealOptions": {"transition":"zoom"},

  "template": "template/reveal.html",
  "listingTemplate": "template/listing.html",

  // 主题设置，可用的主题可以移步至
  // https://gitlab.com/pre-group/styles-src
  "theme": "souler-light",

  "title": "Presentation",
  "separator": "\r?\n---\r?\n",

  // 输出为静态html文件设置，需要设置文件目录。
  "static": false,
  "staticDirs": [],
  
  "verticalSeparator": "\r?\n----\r?\n",
  "watch": false
}

```