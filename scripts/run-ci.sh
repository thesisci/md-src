git config --global user.email "ci@pre-gen.sysu"
git config --global user.name "CI Runner"
git add ../demo.md
git add ../configs.json
git commit -m "check ci"
# 这里需要将 Git Repo 替换成你自己的仓库地址
# 如果使用 SSH 的方式请注意添加 SSH-Key
# 例如 git push https://gitlab.com/pre-group/md-src master
# 或者 git push git@gitlab.com:pre-group/md-src.git master
git push <Insert Git Repo Here> master