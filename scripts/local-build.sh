echo $pwd;
if [ ! -d "scripts-src" ]; then
    git clone https://gitlab.com/pre-group/scripts-src/;
    cd scripts-src;
    npm install;
    cd ..;
fi
cp configs.json scripts-src/src/defaults.json
scripts-src/index.js demo.md --static demo